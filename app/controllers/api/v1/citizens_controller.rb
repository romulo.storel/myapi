class Api::V1::CitizensController < Api::BaseController
  def index
    @citizens = Person.all

    render json: @citizens, each_serializer: CitizenSerializer
  end

  def show
    @citizen = Person.find(params[:id])

    render json: @citizen, serializer: CitizenSerializer
  end
end
